extends CharacterBody2D

@export var walkingSpeed = 200
@export var runningSpeed = 600

# UI variables

var health = 100
var maxHealth = 100
var healthRegeneration = 1
var stamina = 100
var maxStamina = 100
var staminaRegeneration = 5

signal health_updated
signal staminar_updated
signal arrow_pickups_updated
signal health_pickups_updated
signal manna_pickups_updated

@onready var animation_sprite = $AnimatedSprite2D
@onready var healthBar = $"../UI/HealthBar"
@onready var staminaBar = $"../UI/StaminaBar"


# pickups
enum Pickups {ARROW, HEALTH, MANNA } # see Pickup.gd
var pickups = {
	Pickups.ARROW: 0,
	Pickups.HEALTH: 0,
	Pickups.MANNA: 0,
}

func handle_pickup(item: Pickups):
	if item in pickups:
		pickups[item] += 1
		print("Pickup val:" + str(item) + ": " + str(pickups[item]))
		if item == Pickups.ARROW:
			arrow_pickups_updated.emit(pickups[Pickups.ARROW])
		if item == Pickups.HEALTH:
			health_pickups_updated.emit(pickups[Pickups.HEALTH])
		if item == Pickups.MANNA:
			manna_pickups_updated.emit(pickups[Pickups.MANNA])

func _ready():
	health_updated.connect(healthBar.update_health_ui)
	staminar_updated.connect(staminaBar.update_stamina_ui)
	arrow_pickups_updated.connect($"../UI/ArrowAmount".update_arrow_pickup_ui)
	health_pickups_updated.connect($"../UI/HealthPotions".update_health_pickup_ui)
	manna_pickups_updated.connect($"../UI/ManaPotions".update_manna_pickup_ui)

func _process(delta):
	var updated_health = min(health + healthRegeneration * delta, maxHealth)
	if (updated_health != health):
		health = updated_health
		health_updated.emit(health, maxHealth)
	if (!isRunning()):
		var updated_stamina = min(stamina + (staminaRegeneration * delta), maxStamina)
		if (updated_stamina != stamina):
			stamina = updated_stamina
			staminar_updated.emit(stamina, maxStamina)
	
	if Input.is_action_pressed("ui_right"):
		$AnimatedSprite2D.play("walk_right")
	elif Input.is_action_pressed("ui_left"):
		$AnimatedSprite2D.play("walk_left")
	elif Input.is_action_pressed("ui_up"):
		$AnimatedSprite2D.play("walk_up")
	elif Input.is_action_pressed("ui_down"):
		$AnimatedSprite2D.play("walk_down")
	else:
		$AnimatedSprite2D.stop()
	
func _physics_process(delta):
	 # Get player input (left, right, up/down)
	var direction: Vector2
	direction.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	direction.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	# If input is digital, normalize it for diagonal movement
	if abs(direction.x) == 1 and abs(direction.y) == 1:
		direction = direction.normalized()
		
	var moveSpeed = walkingSpeed
	if isRunning():
		moveSpeed = runningSpeed
		stamina = stamina - (10 * delta)
		staminar_updated.emit(stamina, maxStamina)

	# Apply movement
	var movement = moveSpeed * direction * delta
	# Moves our player around, whilst enforcing collisions so that they come to a stop when colliding with another object.
	move_and_collide(movement)

func isMoving():
	return (Input.is_action_pressed("ui_right") || Input.is_action_pressed("ui_left") || 
		Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down"));

func isRunning():
	return isMoving() && Input.is_action_pressed("player_run") && stamina >= 25;

