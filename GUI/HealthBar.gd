# HealthBar.gd

extends ColorRect
@onready var value = $Value

func update_health_ui(health: int, max_health: int):
	value.size.x = 198 * health / max_health

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
