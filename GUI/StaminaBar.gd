# StaminaBar.gd

extends ColorRect
@onready var value = $Value

func update_stamina_ui(stamina: int, max_stamina: int):
	value.size.x = 198 * stamina / max_stamina

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
