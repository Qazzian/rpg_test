@tool

extends Area2D

enum Pickups {ARROW, HEALTH, MANNA }

@export var item : Pickups

var textures = {
	Pickups.ARROW: preload("res://assets/gdm-rpg-icon-pack-32x32-200-icons/Icons/icon186.png"),
	Pickups.HEALTH: preload("res://assets/gdm-rpg-icon-pack-32x32-200-icons/Icons/icon115.png"),
	Pickups.MANNA: preload("res://assets/gdm-rpg-icon-pack-32x32-200-icons/Icons/icon122.png"),
}

# Called when the node enters the scene tree for the first time.
func _ready():
	if not Engine.is_editor_hint():
		if item in textures:
			$Sprite2D.set_texture(textures[item])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Engine.is_editor_hint():
		if item in textures:
			$Sprite2D.set_texture(textures[item])


func _on_body_entered(body):
	if body.name == "Player":
		body.handle_pickup(item)
		get_tree().queue_delete(self)
